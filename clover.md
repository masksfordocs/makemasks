# CLOVER MASK PATTERN

**Note: Significant edits were made to the pattern on April XX, 2020. For the previous version, please click here.**

This pattern is a pleated rectangular “surgical-style” face mask that combines the best elements of well-known patterns such as the Deaconess, A.B. mask (For a nurse, by a nurse), We Can Sew It, the Cynthia, and the Erin.

## Disclaimer

_This particular mask pattern has not been evaluated for effectiveness at protection against viral particles and is NOT intended to be a substitute for medical-grade PPE such as the N95 respirator._

_They can be used in conjunction with medical-grade PPE, or as a last resort when no medical-grade PPE is available._

## Features

*   **Filter pocket** so that the user can insert disposable filter materials to increase the protection this mask provides
*   Provides options for **ties** **OR elastic**
*   **Nose wire pocket** so that the user can insert a metal clip to improve the fit to the user’s face
*   The finished mask is flat, allowing you to pack and ship large quantities efficiently
*   Can be used with contrasting fabric for front/back, or the same fabric
*   The final mask size is approximately 4” x 7” with the pleats closed

## Material Recommendations

*   **Fabric: **This pattern will work for both woven (bed sheets/pillowcases, quilter’s cotton, broadcloth, or muslin) and knit (T-shirt) fabrics, however, we recommend 100% cotton wovens since:
    *   Cotton wovens pleat easily, reducing the need for pinning and saving time
    *   This pattern works off rectangular sections of fabric, so cutting time can be greatly reduced using the ‘notch-and-rip’ method which only works on cotton wovens
    *   Cotton wovens are more beginner-friendly and don’t require specialized needles or stitches
    *   Note: Use lightly colored fabric when possible, since it easily shows stains.
*   **Ties: **Ties can be made from any of the following materials as long as they are at least 0.25” - 0.5” wide between their finished edges. This pattern requires one tie 50” long (users can always shorten, but cannot lengthen ties).
        *   Bias tape (purchased or handmade, must be at most ½” when folded)
        *   Cotton twill tape less than 1” thick
        *   1” strips of jersey knit/t-shirt fabric (go to very end of the document for instructions)
        *   Clean shoelaces or lanyards
        *   Sturdy ribbon that won’t fray, less than 1” thick
        *   Corset lacing
*   **Elastics:** Any elastic between ⅛’’ and ½’’ will work. As elastic is running short, people have been using bungee cords, hairties, beading cord elastic, and others. For earloops, you will need two pieces of 7” lengths. If they are not flat, we recommend knotting the ends so that the thread has something to “grip” and sew into. 
*   **Needle: **We recommend a **size 90/14 needle **(if that's not available, a "jeans" needle). It will help you sew through the many layers of the pleats. (You may also want to swap to a new needle before starting anyway, since they do get less sharp with use).
*   **Optional Nose Clip** (see Step 11)**: **You can use anything from folded over tin foil, to pipe cleaners, to twist ties, to cuttings of aluminum pans, measuring ~4’’. Make sure that the piece you make is bendable and holds its shape, and _any sharp corners are rounded off_. Package any nose wires separately, do not insert into the masks. This will allow the recipient to wash the masks first then insert the nose wires. 

**PREP YOUR MATERIALS**

*   Some recipients prefer that the masks have a different fabric on the outside than inside so that it's easy to tell which side is the front.  If you use this method:

    Cut two 8.5" x 8" rectangles, joined with 1/4" seam to make an 8.5" x 15.75” rectangle) 

    **OR**, if you’re doing the same color on each side, cut one 8.5" x 16" rectangle (Skip to Step 2)           
    

** TODO: insert title materials01.png **
![alt_text](images/Copy-of0.png "image_tooltip")


*   Make a tie about 50” long, 0.25” width. See the very end of this document for instructions. 	

            **OR**, Prepare two elastic pieces at least 7” long. 


**INSTRUCTIONS**


1. Sew the long ends of the rectangles together with a 0.25” seam.

    Open and press the seam. You will now have a rectangle that is 8.5” x ~16” (15.75” if you’re exact).

    ![alt_text](images/seam01.png "image_tooltip")
    

2. With the seam facing up, fold up the long end of the rectangle 1/4", press and stitch in place. Do this to both the top and bottom edge.
    What it should look like after Step 2, ready to be pleated!

3. Prep the fabric to make pleats. 

    Fold the fabric in half, long-wise (the seamed edges should be together), with the wrong sides together. Press. 

    ![alt_text](images/Copy-of2.png "image_tooltip")

    Open it up (there should be a middle fold) and fold the seamed edges towards the middle, like a French door. Press the folds.

    ![alt_text](images/Copy-of3.png "image_tooltip")

    ![alt_text](images/Copy-of4.png "image_tooltip")

    Open it up the entire piece, right side up. 

    ![alt_text](images/Copy-of5.png "image_tooltip")

4. Make three pleats. (The [previous pattern](https://drive.google.com/open?id=1SGdP2lq341kfGCrkM6rL4K_ilKWVXPb6j5NQx-9ML2c) had an additional pleat for four pleats total)

    Pinch each fold over and down to make 3 pleats (see photos). 

    Remember, it doesn't have to be perfect!

    <table>
      <tr>
        <td>
          <img src="images/Copy-of6.png" width="" alt="alt_text" title="image_tooltip">
          <p/>
          <img src="images/Copy-of7.png" width="" alt="alt_text" title="image_tooltip">
          <p/>
          <img src="images/Copy-of8.png" width="" alt="alt_text" title="image_tooltip">
        </td>
        <td><strong><span style="text-decoration:underline;">PLEATING TIPS</span></strong>
          <p/>
          <strong><em>This pleating trick is based off of <a href="https://youtu.be/TL9D6ZFtZHM">Sweet Red Poppy’s video</a> at the 1:41 mark.</em></strong>
          <ul>
             <li>Use pins or magic clips to hold down the pleats in the beginning. After a while, you won’t need to use them.</li>
             <li>It was <em>slightly</em> easier to start from the bottom and fold each pleat on top of each other (but you can start at the top!)</li>
             <li>Lightly press the pleats once you’re done.</li>
          </ul>
        </td>
      </tr>
    </table>

    *OPTIONAL BUT HIGHLY RECOMMENDED: *
    Sew down the seams so that the pleats stay pleated for the remainder of the pattern. (Note: you can make these basting stitches and remove them when the rest of the seams are in place.)
    ![alt_text](images/Copy-of9.png "image_tooltip")
    
    Your mask should now look like this, right side facing you, pleats pointing down.
    ![alt_text](images/Copy-of10.png "image_tooltip")

5. Fold in half along the seam so that the right sides are together. Make sure the bottom of the mask is even on each side.
    ![alt_text](images/Copy-of11.png "image_tooltip")

6. Sew the short edge with a 0.25” seam. 
    ![alt_text](images/Copy-of12.png "image_tooltip")

7. Flip the mask right side out and press. You will now have a rectangle that is about 7-8” wide and 3-4” tall (depending on your pleats). 
    ![alt_text](images/Copy-of13.png "image_tooltip")

8. Orient the mask so that the pleats are facing down. 
    On each side, sew from top to bottom about 0.5” - ⅝” from the sides. _These are the tunnels for the tie to pass through._

    ![alt_text](images/Copy-of14.png "image_tooltip")
    ![alt_text](images/Copy-of15.png "image_tooltip")

    _Sewing the tunnels of the mask where the tie/elastic will go._

9. Sew closed the top of the mask from one tunnel seam to the other.
    ![alt_text](images/Copy-of16.png "image_tooltip")
		
10. Add about 1 inch of stitching on the bottom edge from each tunnel seam on each side to help hold the filter in. (Make sure you don’t sew the opening of the tunnel closed!)
    ![alt_text](images/Copy-of17.png "image_tooltip")


    ```HOT TIP: Sew steps 8-10 in one continuous stitch (Blue lines in the figure). 

    First, starting from the bottom of one side, stitch the tunnel for that side, followed by the sewing of the top closed, then down the tunnel for the opposite side, then sewing one side of the filter pocket (Step 10). 

    Then sew the other side of the filter pocket (pink line). ```

    ![alt_text](images/Copy-of18.png "image_tooltip")

    _This is what your mask should look like after sewing the tunnel seams, the top of the mask closed, and the bottom stitches to hold the filter in._

11. Add nose wire pocket. 
    Stitch a line about 0.75” from the top edge 3” long, making sure to backstitch the ends. _(The pin is there to mark the middle line of the mask so that the pocket is centered)._
    ![alt_text](images/Copy-of19.png "image_tooltip")


12. Insert tie DOWN one side and UP the other so that the tie forms a U at the bottom of the mask. The open edge of the mask should be on the same side of the U. 
    **_This step was revised on 4/3/20. Previous pattern [here](https://docs.google.com/document/d/1n-f_-I3B4vNFLU6slP5Zo6lcStWrj3DJXO6UKl7h8vY/edit?usp=sharing)._**

    (For elastic earloops, see Pg 16) 


### Tips for threading the ties, per our community
    
![alt_text](images/Copy-of20.png "image_tooltip")

![alt_text](images/Copy-of21.png "image_tooltip")


**YOUR MASK IS FINISHED!**

**P.S. **Here is a [batching process ](https://www.instructables.com/id/Face-Mask-Batch-Process/)developed by our sewist Christine K for even speedier construction.

**P.P.S.** If you used a single piece of fabric, you may want to add embellishment (e.g. a strip of ric rac) to one side so the wearer can distinguish from from back.

### HOW TO MODIFY FOR ELASTIC EARLOOP

*   Use two 14” strips of elastic. (These may fit a little loose on the end user, but they can knot them to their liking). 
*   Insert into channels and knot to create a loop.
*   Please note: we have received feedback from facilities that ties are preferable to earloops as earloops can irritate behind the ears after long use
    ![alt_text](images/Copy-of22.png "image_tooltip")

### HOW TO MAKE FABRIC TIES

*   Cut a strip of fabric 1” x 50” (this is approximate - the length can be anywhere from 45” and upwards)
*   Fold the long edges in towards the middle while pressing, then fold the entire thing in half (will be like a hardcover book sleeve). It will be 4 layers.   Press. 
*   Sew down the middle of the tie. Done!

**HOT TIP: **You can [request a 3D-printed bias jig](https://forms.gle/XmAk6VspPQozw9SF8) from one of our community members to expedite this process.

### SOME TIPS FOR SPEED SEWING / EFFICIENCY

#### Threads & Bobbins

*   Thread doesn’t have to match the fabric! 
*   Relatedly, the bobbin thread doesn’t need to match the top thread.
    (They, however, need to be the same weight)

*   Wind 5 or 6 bobbins before you start sewing to minimize time spent threading and unthreading the main spool. So, when you run out of bobbin thread, you can just pop in the next one.

#### Chain-piecing / Batch-making

*   Make several masks at one time by doing each step for multiple masks. 

    For example, you can do Step 1 for many pieces of fabric. Then you can move onto Step 2 for all the pieces. And so on. This is especially helpful in the pleating step where you can fold and pin all the fabrics before taking them all to be pressed and sewn. 

*   Combine steps. 

    For example, pressing the seam at the end of Step 1 can be combined with Step 2 (folding in the long edge and pressing)

    Another example: Step 9 can be combined with Step 10 so that you can continuously sew the tunnel and filter “holder” for one side.

*   To minimize cutting of threads, simply pull the piece you’re finished sewing away from the needle about 1” WITHOUT cutting, and continue sewing the next piece. Cut all the threads at the end of the batch. All you will have is maybe 1/4" (at most) of thread between most pieces, and only a longer thread when you start/stop a long chain of pieces.

#### OTHER TIPS

*   The tie don’t need to be as high quality material as the mask itself. If you have cotton-poly blend fabric, feel free to use it for ties!

