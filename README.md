# README #

This repository contains the Markdown source files for the [MakeMasks group](https://makemasks2020.org).
MakeMasks is a grassroots collective seeking to provide short-term relief by mobilizing sewists,
makers, and organizers to hand-sew and deliver masks to those affected by mask shortages.

### What is this repository for? ###

* We use this repository to store documentation files and their translations into various languages.
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)
